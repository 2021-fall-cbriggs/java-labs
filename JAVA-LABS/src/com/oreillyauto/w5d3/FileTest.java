package com.oreillyauto.w5d3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class FileTest {
    public FileTest() {
        // The default classpath is the actual project. It doesn't matter in what package you're
        // running your code. Since we have numbers.txt at the root of the project, we can access it
        // without any other reference. 
        File file = new File("numbers.txt");
        System.out.println("file.exists (root location): " + (file.exists()));
        // Output: file.exists (root location): true

        File file2 = new File(getClass().getResource("/com/oreillyauto/w5d3/numbers.txt").getFile());
        System.out.println("file2.exists (package): " + (file2.exists()));
        // Output: file2.exists (package): true
        //calc(file);
    }

    public void calc(File file) throws NumberFormatException, IOException {
        // Integer.parseInt() Integer.valueOf()
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(file));
            String str;
            int sum = 0;

            while ((str = br.readLine()) != null) {
                String[] values = str.replaceAll("^\"", "").split("\"?(,|$)(?=(([^\"]*\"){2})*[^\"]*$) *\"?");
                for (String string : values) {
                    sum += Integer.parseInt(string);
                }
            }

            System.out.println("Sum: " + sum);
        }
        finally {
            try {
                br.close();
            }
            catch (Exception e) {//do nothing
            }
        }

    }

    public static void main(String[] args) {
        new FileTest();

    }
}
