package com.oreillyauto.W6D1;

public class Cards {
    private static final String KING = "King";
    private static final String QUEEN = "Queen";
    private static final String JACK = "Jack";
    private static final String TEN = "Ten";
    private static final String NINE = "Nine";
    private static final String EIGHT = "Eight";
    private static final String SEVEN = "Seven";
    private static final String SIX = "Six";
    private static final String FIVE = "Five";
    private static final String FOUR = "Four";
    private static final String THREE = "Three";
    private static final String TWO = "Two";
    private static final String ACE = "Ace";
    
    private static final String SPADE = "Spade";
    private static final String CLUB = "Club";
    private static final String HEART = "Heart";
    private static final String DIAMOND = "Diamond";
}
