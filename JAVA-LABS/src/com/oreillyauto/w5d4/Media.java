package com.oreillyauto.w5d4;

import java.sql.Date;

import com.oreillyauto.w5d4.Interfaces.Genre;

public abstract class Media implements Genre{
    private String creator = "God";
    private Date releaseDate = new Date(0);
    
    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title = "";
  
    public Media() {
    }

    public String toString() {
        return "This is Media!";
    }
}
