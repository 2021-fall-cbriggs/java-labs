package com.oreillyauto.w5d2;
/* Un-comment this class. Correct all mistakes. */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Exercise2 {

    List<Integer> intList = new ArrayList<Integer>();
    
    public void LabTwoExercise2() {
        logMessage("Starting process...");
        populateList();
        iterateList();
        logMessage("Process Complete.");
    }

    private void populateList() {
        for (int i = 0; i < 5; i++) {
            intList.add(i);
        }
    }

    private void iterateList() {
        for (Integer integer : intList) {
            System.out.println(integer);
        }
    }

    public static void main(String[] args) {        
        Exercise2 ltet = new Exercise2();
        logStaticMessage("Done", "%s");
    }

    private static void logMessage(String message) {
        System.out.printf("%d " + (message + "\n") , new Date());
    }
    
    private static void logStaticMessage(String message, String thing) {
        System.out.printf(thing + (message + "\n") , new Date());
    }
    
}