package com.oreillyauto.w5d4.TVShows;

import com.oreillyauto.w5d4.Media;

public class TVShow extends Media {
    private int numSeasons = 0;
    private int[] episodes = new int[1];
    
    public TVShow(int numSeasons, int[] episodes) {
        this.numSeasons = numSeasons;
        this.episodes = episodes;
    }

    public int getNumSeasons() {
        return numSeasons;
    }

    public void setNumSeasons(int numSeasons) {
        this.numSeasons = numSeasons;
    }

    public int[] getEpisodes() {
        return episodes;
    }

    public void setEpisodes(int[] episodes) {
        this.episodes = episodes;
    }
    
    
}
