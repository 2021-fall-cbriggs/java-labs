package com.oreillyauto.w5d4.Movies;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.oreillyauto.w5d4.Media;
import com.oreillyauto.w5d4.Interfaces.Digital;
import com.oreillyauto.w5d4.Interfaces.Genre;

public class Movie extends Media implements Digital{
    private String director = "";
    private int runtimeInMinutes = 0;
    private List<String> actors = new ArrayList<String>();
    private BigDecimal boxOffice = new BigDecimal("0");
    
    public Movie(String director, int runtimeInMinutes, List<String> actors, BigDecimal boxOffice) {
        super();
        this.director = director;
        this.runtimeInMinutes = runtimeInMinutes;
        this.actors = actors;
        this.boxOffice = boxOffice;
    }

    public Movie() {
        super();
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getRuntimeInMinutes() {
        return runtimeInMinutes;
    }

    public void setRuntimeInMinutes(int runtimeInMinutes) {
        this.runtimeInMinutes = runtimeInMinutes;
    }

    public List<String> getActors() {
        return actors;
    }

    public void setActors(List<String> actors) {
        this.actors = actors;
    }

    public BigDecimal getBoxOffice() {
        return boxOffice;
    }

    public void setBoxOffice(BigDecimal boxOffice) {
        this.boxOffice = boxOffice;
    }
    
    public String toString() {
        return "This is a Movie!";
    }
    
    
}
