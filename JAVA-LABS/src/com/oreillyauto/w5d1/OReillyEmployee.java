package com.oreillyauto.w5d1;

import java.math.BigDecimal;

public class OReillyEmployee {
    private Long id;
    private int age = 0;
    private String name;
    private BigDecimal salary;
    private String title;
   
    public OReillyEmployee(Long id, int age, String name, BigDecimal salary, String title) {
        super();
        this.id = id;
        this.age = age;
        this.name = name;
        this.salary = salary;
        this.title = title;
    }
    
    public OReillyEmployee() {
        super();
    }
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public BigDecimal getSalary() {
        return salary;
    }
    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

}
